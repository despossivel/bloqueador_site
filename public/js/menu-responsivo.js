var menuElements = document.getElementById("navegacao");
menuElements.insertAdjacentHTML("afterend", "<a href='#' id='botaomenu' class='icon-menu'></a>");
menuElements.className = "desativa";

document.getElementById("botaomenu").onclick = function() {

	if(menuElements.getAttribute("class") == "desativa"){
		menuElements.className = "ativa";
		document.getElementById("botaomenu").className = "icon-cancel";
	}else{
		menuElements.className = "desativa";
		document.getElementById("botaomenu").className = "icon-menu";
	}
	event.preventDefault();

};