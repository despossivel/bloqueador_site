$(document).ready(function () {

    // GERAL //
    
      $('header nav li.menu-item-has-children > a, header nav li.page_item_has_children > a').click(function(){
        if($(window).width() < 992){
          if(!$(this).next().hasClass('ativa')){
            $('li.menu-item-has-children > ul, li.page_item_has_children > a').removeClass('ativa');
            $(this).next().addClass('ativa');
          }else{
            $('li.menu-item-has-children > ul, li.page_item_has_children > a').removeClass('ativa');
          }
        }
        return false;
      });
    
      $('header nav li.page-item-28 > a').click(function(){
        // $('.lente').fadeIn(750);
        // $('html').css({'overflow-y':'hidden'});
      });
    
        $('.lente a, .lente').click(function(){
            $('.lente').fadeOut(750, function(){
          $('#video').remove(); 
        });
        $('html').css({'overflow-y':'scroll'});
            return false;
        });
    
      $('.home header nav a').click(function(){
        $('html,body').animate({scrollTop: $('main[url="'+ $(this).attr('href') +'"]').offset().top - ($('header h1').height() + 80) }, {duration: 500, queue: false});
    
        $('#navegacao').addClass("desativa").removeClass('ativa');
        $('#botaomenu').addClass("icon-menu").removeClass('icon-cancel');
    
        window.history.pushState(null, document.title, $(this).attr('href') );
    
        if($('main[url="'+ $(this).attr('href') +'"] .lente').length > 0) $('main[url="'+ $(this).attr('href') +'"] .lente').fadeIn(500);
    
        return false;
      });
    
      $('[data-fancybox]').fancybox();
      $('.item-altura').matchHeight();
    
    // INICIAL
      $('#inicial #banners #slider').slick({
        dots: false,
        arrows: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 5000,
        adaptiveHeight: true,
        fade: true
      });
    
    // ONDE COMPRAR
    
      $('body').on('click', '#onde-comprar #estados a', function(){
          var form = $('.etapas.etapa1').attr('action');
          var estado_escolhido = $(this).attr('estado');
    
          $('.etapas.etapa2').slideUp(250);
    
          $.ajax({
            method: "POST",
            url: form,
            data: { estado: estado_escolhido }
          }).done(function( html ) {
    
            $('.etapas.etapa2').html(html).slideDown(750);
            $('html,body').animate({scrollTop: $('.etapas.etapa2').offset().top - ($('header h1').height() + 80) }, {duration: 500, queue: false});
    
          });
    
          return false;
      });
      $('body').on('click', '#onde-comprar #cidades a', function(){
          var form = $('.etapas.etapa1').attr('action');
          var cidade_escolhido = $(this).attr('cidade');
          var estado_escolhido = $(this).attr('estado');
    
          $('.etapas.etapa3').slideUp(250);
    
          $.ajax({
            method: "POST",
            url: form,
            data: { cidade: cidade_escolhido, estado_cidade:estado_escolhido }
          }).done(function( html ) {
    
    
            $('.etapas.etapa3').html(html).slideDown(750, function(){ $('.item-altura').matchHeight(); });
            $('html,body').animate({scrollTop: $('.etapas.etapa3').offset().top - ($('header h1').height() + 80) }, {duration: 500, queue: false});
          });
    
          return false;
      });
    
    // FAQ
      $('#perguntas-frequentes .pergunta').click(function(){
        $('.resposta').slideUp(250);
        $(this).next('.resposta').slideDown(500, function(){
          $('html,body').animate({scrollTop: $(this).offset().top - ($('header h1').height() + 40) }, {duration: 500, queue: false});
        });
    
        return false;
      });
    
    // CLIENTES & RESULTADOS
      $('#contato .controle a').click(function(){
        $('#l1, #l2').slideUp(250);
        $('#l'+ $(this).attr('form')).slideDown(500);
    
        console.log( $(this).attr('form') );
    
        return false;
      });
    
    });